import * as moment from 'moment';

export const checkDate = (date: string) => moment(date).isValid();
