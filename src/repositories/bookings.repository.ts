import { EntityRepository, AbstractRepository, DeepPartial } from 'typeorm';
import { Booking } from 'src/entities/booking.entity';
import { ICreateBooking } from 'src/rooms/types/create-booking.interface';

@EntityRepository(Booking)
export class BookingRepository extends AbstractRepository<Booking> {
  async create(createBooking: DeepPartial<ICreateBooking>) {
    const row = await this.repository.create(createBooking);

    return await this.manager.save(row);
  }
}
