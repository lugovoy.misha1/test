import { AbstractRepository, Brackets, EntityRepository } from 'typeorm';
import { Room } from 'src/entities/room.entity';
import { BookingPeriodDto } from 'src/rooms/dto/booking-period.dto';

@EntityRepository(Room)
export class RoomRepository extends AbstractRepository<Room> {
  private findFreeRoom({ fromDate, toDate }: BookingPeriodDto) {
    return this.repository.createQueryBuilder('room').where((qb) => {
      const subQuery = qb
        .subQuery()
        .select('room.id')
        .from(Room, 'room')
        .innerJoin('room.bookings', 'bookings')
        .orWhere(
          new Brackets((qb) => {
            qb.where(`"bookings"."fromDate" BETWEEN :fromDate AND :toDate`, {
              fromDate,
              toDate,
            }).orWhere(`"bookings"."toDate" BETWEEN :fromDate AND :toDate`, {
              fromDate,
              toDate,
            });
          }),
        )
        .getQuery();
      return 'room.id NOT IN ' + subQuery;
    });
  }
  getFreeRooms(period: BookingPeriodDto) {
    return this.findFreeRoom(period).getMany();
  }

  getFreeRoomById(id: number, period: BookingPeriodDto) {
    return this.findFreeRoom(period).andWhere('room.id = :id', { id }).getOne();
  }
}
