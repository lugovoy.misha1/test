import { IRoom } from './room.interface';

export interface ICreateBooking {
  fromDate: string;
  toDate: string;
  room: IRoom;
}
