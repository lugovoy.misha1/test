import { IsNumber } from 'class-validator';
import { BookingPeriodDto } from './booking-period.dto';

export class BookRoomDto extends BookingPeriodDto {
  @IsNumber()
  id: number;
}
