import { IsDateString } from 'class-validator';

export class BookingPeriodDto {
  @IsDateString()
  fromDate: string;

  @IsDateString()
  toDate: string;
}
