import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { RoomsService } from './rooms.service';
import { RoomsController } from './rooms.controller';
import { RoomRepository } from 'src/repositories/room.repository';
import { BookingRepository } from 'src/repositories/bookings.repository';

@Module({
  imports: [TypeOrmModule.forFeature([RoomRepository, BookingRepository])],
  providers: [RoomsService],
  controllers: [RoomsController],
})
export class RoomsModule {}
