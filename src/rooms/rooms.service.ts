import { BadRequestException, Injectable } from '@nestjs/common';
import { BookingRepository } from 'src/repositories/bookings.repository';
import { RoomRepository } from 'src/repositories/room.repository';
import { checkDate } from 'src/utils/check-date';
import { BookRoomDto } from './dto/book-room.dto';
import { BookingPeriodDto } from './dto/booking-period.dto';

@Injectable()
export class RoomsService {
  constructor(
    private roomRepository: RoomRepository,
    private bookingRepository: BookingRepository,
  ) {}

  private checkPeriod({ fromDate, toDate }: BookingPeriodDto) {
    if (!checkDate(fromDate) || !checkDate(toDate)) {
      throw new BadRequestException(
        'fromDate or toDate must be a valid ISO 8601 date string ',
      );
    }

    if (fromDate > toDate) {
      throw new BadRequestException(
        'toDate must be equal or bigger than fromDate',
      );
    }
  }

  getFreeRooms(period: BookingPeriodDto) {
    this.checkPeriod(period);
    return this.roomRepository.getFreeRooms(period);
  }

  async bookRoom({ id, ...period }: BookRoomDto) {
    this.checkPeriod(period);
    const room = await this.roomRepository.getFreeRoomById(id, period);

    if (!room) {
      throw new BadRequestException('This room is not available');
    }

    await this.bookingRepository.create({ ...period, room });
    return { roomId: id };
  }
}
