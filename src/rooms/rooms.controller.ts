import { Body, Controller, Get, Post, Query } from '@nestjs/common';
import { ApiQuery, ApiTags } from '@nestjs/swagger';
import { BookRoomDto } from 'src/rooms/dto/book-room.dto';
import { BookRoomResponseDto } from './dto/book-room-response.dto';
import { BookingPeriodDto } from './dto/booking-period.dto';
import { RoomDto } from './dto/room.dto';
import { RoomsService } from './rooms.service';

@ApiTags('Rooms')
@Controller('rooms')
export class RoomsController {
  constructor(private roomsService: RoomsService) {}

  @Get()
  @ApiQuery({ type: String, name: 'fromDate', example: '2020-02-02' })
  @ApiQuery({ type: String, name: 'toDate', example: '2020-02-02' })
  getFreeRooms(@Query() query: BookingPeriodDto): Promise<RoomDto[]> {
    return this.roomsService.getFreeRooms(query);
  }

  @Post('booking')
  bookRoom(@Body() body: BookRoomDto): Promise<BookRoomResponseDto> {
    return this.roomsService.bookRoom(body);
  }
}
